const router = require('express').Router();
const DbWorker = require('../services/db-worker');

router.route('/')
    .get(async function (req, res) {
        try {
            const orderIngredients = await DbWorker.getAll();
            await res.status(200).json(orderIngredients);
        } catch (err) {
            return res.status(500).send(err);
        }
    })
    .post(async function (req, res) {
        try {
            const newData = await DbWorker.create(req.body);
            await res.status(201).json(newData);
        } catch (err) {
            return res.status(500).send(err);
        }
    });

router.route('/:orderIngredientId')
    .get(async function (req, res) {
        try {
            const orderIngredients = await DbWorker.getOneById(req.params.orderIngredientId);
            await res.status(200).json(orderIngredients);
        } catch (err) {
            return res.status(500).send(err);
        }
    })
    .put(async function (req, res) {
        try {
            const newData = await DbWorker.update(req.params.orderIngredientId, req.body);
            await res.status(200).json(newData);
        } catch (err) {
            return res.status(500).send(err);
        }
    })
    .delete(async function (req, res) {
        try {
            const delData = await DbWorker.delete(req.params.orderIngredientId);
            await res.status(200).json(delData);
        } catch (err) {
            return res.status(500).send(err);
        }
    });

module.exports = router;
